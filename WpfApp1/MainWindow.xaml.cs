﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<ViewModel> data = new List<ViewModel>();

        public MainWindow()
        {
            InitializeComponent();

            SetData();
        }

        async void SetData()
        {
            await Task.Run(() =>
            {
                for (int i = 0; i < 2500; i++)
                {
                    data.Add(new ViewModel(i + 1));
                }
            });

            Lv.ItemsSource = data;
        }

        class ViewModel
        {
            public string Info1 { get; set; }
            public string Info2 { get; set; }
            public string Info3 { get; set; }
            public string Info4 { get; set; }
            public string Info5 { get; set; }
            public string Info6 { get; set; }
            public string Info7 { get; set; }
            public string Info8 { get; set; }
            public string Info9 { get; set; }
            public string Info10 { get; set; }
            public string Info11 { get; set; }
            public string Info12 { get; set; }
            public string Info13 { get; set; }

            public ViewModel(int id)
            {
                Info1 = id.ToString();
                Info2 = Guid.NewGuid().ToString();
                Info3 = Guid.NewGuid().ToString();
                Info4 = Guid.NewGuid().ToString();
                Info5 = Guid.NewGuid().ToString();
                Info6 = Guid.NewGuid().ToString();
                Info7 = Guid.NewGuid().ToString();
                Info8 = Guid.NewGuid().ToString();
                Info9 = Guid.NewGuid().ToString();
                Info10 = Guid.NewGuid().ToString();
                Info11 = Guid.NewGuid().ToString();
                Info12 = Guid.NewGuid().ToString();
                Info13 = Guid.NewGuid().ToString();
            }
        }

        private void Btn_Edit_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Btn_Delete_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Lv_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

        }

        private void Lv_CopyInfo(object sender, RoutedEventArgs e)
        {

        }

        private void Lv_Header_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Lv_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {

        }

        private void Lv_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {

        }
    }

}
